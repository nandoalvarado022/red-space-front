import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import './styles.scss';

function List(props) {
  const { data, handleChange, loading } = props

  return (
    <div className="List">
      <Autocomplete
        disabled={loading}
        onChange={(e, value) => handleChange(value)}
        disablePortal
        id="combo-box-demo"
        options={data}
        sx={{ width: 300 }}
        renderInput={(params) => <TextField {...params} label="Select character" />}
      />

    </div>
  );
}

export default List;
