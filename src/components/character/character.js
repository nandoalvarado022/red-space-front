import './styles.scss';

function Character(props) {
    const { name,
        height,
        mass,
        hair_color,
        skin_color,
        gender,
        birth_year,
        home_planet,
        films } = props.characterSelected
    return (
        <div className="Character">
            <div className="Card">
                <div>
                    <span>Name</span>
                    <span>{name}</span>
                </div>
                <div>
                    <span>Height</span>
                    <span>{height}</span>
                </div>
                <div>
                    <span>Mass</span>
                    <span>{mass}</span>
                </div>
                <div>
                    <span>Hair Color</span>
                    <span>{hair_color}</span>
                </div>
                <div>
                    <span>Skin Color</span>
                    <span>{skin_color}</span>
                </div>
                <div>
                    <span>Gender</span>
                    <span>{gender}</span>
                </div>
                <div>
                    <span>Birth Year</span>
                    <span>{birth_year}</span>
                </div>
                <div>
                    <span>Home Planet</span>
                    <span>
                        <span>{home_planet?.title}</span>
                        <span>{home_planet?.population}</span>
                        <span>{home_planet?.terrain}</span>
                    </span>
                </div>
                <div className='films_content'>
                    <span>Films</span>
                    <span>
                        {films && films.map(({ title, director, producer, release_date }) => {
                            return <li>
                                <span>{title}</span>
                            </li>
                        })}
                    </span>
                </div>
            </div>
        </div>
    );
}

export default Character;
