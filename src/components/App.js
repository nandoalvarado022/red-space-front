import { useEffect, useState } from 'react'
import Character from './character/character'
import List from './list/List'
import './App.scss'

function App() {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false)
  const [characterSelected, setCharacterSelected] = useState({ id: null })

  const get = async () => {
    const req = await fetch(`http://localhost:3001/character/`)
    const res = await req.json()
    const data = res.map(item => ({ character_id: item.character_id, label: item.name }))
    setData(data)
  }

  useEffect(() => {
    get()
  }, [])

  const handleChange = async ({ character_id }) => {
    setLoading(true)
    try {
      const req = await fetch(`http://localhost:3001/character/${character_id}`)
      const res = await req.json()
      setCharacterSelected(res[0])
      setLoading(false)
    }
    catch (err) {
      setLoading(false)
    }
  }

  return (
    <div className="App">
      <List data={data} handleChange={handleChange} loading={loading}/>
      <Character characterSelected={characterSelected} />
    </div>
  );
}

export default App;
